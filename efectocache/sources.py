#! /usr/bin/env python

import requests, feedparser, tempfile, json, collections
import logging, hashlib, os.path

logger = logging.getLogger('efectocache.sources')

RSS_SOURCES = [
    'http://www.meneame.net/rss2.php?status=queued',
]

Link = collections.namedtuple('Link', ('url', 'title', 'meta'))

def process_rss(url):
    headers = {}
    url_state_file = os.path.join( tempfile.gettempdir(), hashlib.sha512(url).hexdigest() )
    try:
        state = json.load(open(url_state_file, 'r'))
        if 'etag' in state:
            headers['etag'] = state['etag']
        if 'last-modified' in state:
            headers['if-modified-since'] = state['last-modified']
    except:
        state = {}
    resp = requests.get(url,headers=headers)
    feed = feedparser.parse(resp.text)
    links = []
    for entry in feed.entries:
        meta = {}
        entry_link = entry.link
        if 'meneame_link_id' in entry:
            meta['meneame_link_id'] = entry.meneame_link_id
        if 'meneame_url' in entry:
            meta['meneame_link'] = entry_link   
            entry_link = entry.meneame_url
        link = Link( entry_link, entry.title, meta)
        links.append(link)
    try:
        if 'etag' in resp.headers:
            state['etag'] = resp.headers['etag']
        if 'last-modified' in resp.headers:
            state['last-modified'] = resp.headers['last-modified']
        if state:
            json.dump(state, open(url_state_file, 'w'))
    except:
        logger.warning('can\'t save the source statedata')
    return links

def main():
    for source in RSS_SOURCES:
        logger.info('source %s'%source)
        try:
            links = process_rss(source)
        except:
            logger.error('Error processing the source: %s'%source, exc_info=True)
            continue
 
if __name__ == '__main__':
    logging.basicConfig()
    logger.setLevel(logging.DEBUG)
    main()
